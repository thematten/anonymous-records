{-# options_ghc -fplugin=AnonymousRecords #-}

import AnonymousRecords
import Optics.Core

main :: IO ()
main = pure ()

test :: Rec { b :: Int, a :: String, c :: [Float] }
test  = Rec{ a = 5, b = "a", c = [1,2,3] }

-- c :: { c :: [Float] } |> r => Rec r -> [Float]
-- c = view $ field' @"c" @[Float]