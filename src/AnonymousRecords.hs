{-# language RankNTypes, MultiParamTypeClasses #-}

module AnonymousRecords (module M) where

import AnonymousRecords.Plugin   as M (plugin)
import AnonymousRecords.Internal as M (
    Row, RNil, RExt
  , Rec, pattern Rec
  , type (|>)
  , field'
  )
