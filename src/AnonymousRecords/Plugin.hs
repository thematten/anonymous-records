{-# language TemplateHaskell #-}

module AnonymousRecords.Plugin (plugin) where

import           Control.Monad
import           Convert
import           Data.Generics        hiding (empty)
import           Data.Map.Strict      (Map)
import qualified Data.Map.Strict as M
import           ErrUtils
import           GhcPlugins           hiding (Type)
import           HsSyn

import AnonymousRecords.Internal (RExt, RNil, Rec (RN, RE))

-------------------------------------------------------------------------------
plugin :: Plugin
plugin = defaultPlugin{
    pluginRecompile    = purePlugin
  , parsedResultAction = \_ _ -> translateAction
  }

-------------------------------------------------------------------------------
translateAction :: HsParsedModule -> Hsc HsParsedModule
translateAction m@HsParsedModule{ hpm_module } = do
  let translateRecs, translateRows :: Data a => a -> Hsc a
      translateRecs = mkM \case
        L loc (RecordCon _ name bs) :: LHsExpr GhcPs
           | L _ (occName -> occNameFS -> "Rec") <- name
          -> bindsToRec loc bs
        x -> pure x
      translateRows = mkM \case
        (L _ (HsRecTy _ ds) :: LHsType GhcPs)
          -> decsToRow ds
        x -> pure x
  hpm_module' <- everywhereM (translateRows <=< translateRecs) hpm_module
  pure m{ hpm_module = hpm_module' }

-------------------------------------------------------------------------------
bindsToRec :: SrcSpan -> HsRecordBinds GhcPs -> Hsc (LHsExpr GhcPs)
bindsToRec loc = \case
  HsRecFields binds  Nothing -> pure $
    foldr (nlHsApp . uncurry reExpr) rnExpr $ bindsToFields binds
  HsRecFields _      Just{}  -> customError loc
    $   quotes (text "RecordWildCards")
    <+> text "are not supported (yet) with" <+> quotes (text "Rec")
    <+> "pattern"

-------------------------------------------------------------------------------
bindsToFields :: [LHsRecField GhcPs (LHsExpr GhcPs)]
              -> [(FastString, LHsExpr GhcPs)]
bindsToFields = map \case
  L _ (HsRecField (L _ field) val _)
     | FieldOcc _ (L _ (occName -> occNameFS -> name)) <- field
    -> (name, val)

-------------------------------------------------------------------------------
reExpr :: FastString -> LHsExpr GhcPs -> LHsExpr GhcPs
reExpr name = mkHsApp $ noLoc
            $ HsAppType nameApp
            $ nlHsVar $ head $ thRdrNameGuesses 'RE
 where
  nameApp = HsWC NoExt $ noLoc $ HsTyLit NoExt $ HsStrTy NoSourceText name

-------------------------------------------------------------------------------
rnExpr :: LHsExpr GhcPs
rnExpr = nlHsVar $ head $ thRdrNameGuesses 'RN

-------------------------------------------------------------------------------
decsToRow :: [LConDeclField GhcPs] -> Hsc (LHsType GhcPs)
decsToRow = fmap fieldsToRow . flip foldM M.empty \map_ dec -> do
  let (loc, name, typ) = decToField dec
  case M.insertLookupWithKey (\_ a _ -> a) name typ map_ of
    (Nothing,   map_') -> pure map_'
    (Just typ', _    ) -> customError loc
      $   text "Field" <+> quotes (ppr name <+> dcolon <+> ppr typ')
      <+> text "overlaps with" <+> quotes (ppr name <+> dcolon <+> ppr typ)


-- decsToRow decs = do
--   let add map_ (loc, name, type_) =
--         case M.insertLookupWithKey (\_ a _ -> a) name type_ map_ of
--           (Nothing,     map_') -> pure map_'
--           (Just type_', _    ) -> customError loc
--             $   text "Field"
--             <+> quotes (ppr name <+> dcolon <+> ppr typ')
--             <+> text "overlaps with"
--             <+> quotes (ppr name <+> dcolon <+> ppr typ)
--   foldM_ (add . decToField) M.empty decs
--   pure $

-------------------------------------------------------------------------------
decToField :: LConDeclField GhcPs -> (SrcSpan, FastString, LHsType GhcPs)
decToField (L loc dec)
  | ConDeclField _ [L _ field] typ _                <- dec
  , FieldOcc _ (L _ (occName -> occNameFS -> name)) <- field
  = (loc, name, typ)

-------------------------------------------------------------------------------
fieldsToRow :: Map FastString (LHsType GhcPs) -> LHsType GhcPs
fieldsToRow = foldr (nlHsAppTy . uncurry rExtType) rNilType . M.toAscList

-------------------------------------------------------------------------------
rExtType :: FastString -> LHsType GhcPs -> LHsType GhcPs
rExtType name = nlHsAppTy
              $ nlHsAppTy rExtConType $ noLoc
              $ HsTyLit NoExt
              $ HsStrTy NoSourceText name
 where
  rExtConType = nlHsTyVar $ head $ thRdrNameGuesses ''RExt

-------------------------------------------------------------------------------
rNilType :: LHsType GhcPs
rNilType = nlHsTyVar $ head $ thRdrNameGuesses ''RNil

-------------------------------------------------------------------------------
customError :: SrcSpan -> SDoc -> Hsc a
customError loc doc = do
  df <- getDynFlags
  throwOneError $ mkPlainErrMsg df loc doc