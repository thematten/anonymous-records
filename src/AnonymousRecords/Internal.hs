{-# language AllowAmbiguousTypes #-}

module AnonymousRecords.Internal (
    Row, RNil, RExt
  , Rec (..), pattern Rec
  , type (|>)
  , field'
  ) where

import Data.Kind
import Data.List
import GHC.OverloadedLabels
import Data.Proxy
import GHC.TypeLits
import Optics.Lens
import Optics.Label

-------------------------------------------------------------------------------
-- | INVARIANT: does not have duplicate names (to make sure it holds, use
-- literals processed by plugin when constructing one)
data Row n t = RNil | RExt n t (Row n t)

type RNil = 'RNil
type RExt = 'RExt

type Row' = Row Symbol Type

-------------------------------------------------------------------------------
data Rec :: Row' -> Type where
  RN ::                             Rec RNil
  RE :: forall n t r. t -> Rec r -> Rec (RExt n t r)

class ShowFields (r :: Row') where
  showFields :: Rec r -> [String]

instance ShowFields RNil where
  showFields _ = []

instance (KnownSymbol n, Show t, ShowFields r) => ShowFields (RExt n t r) where
  showFields (RE a r) =
    (symbolVal (Proxy @n) ++ " = " ++ show a) : showFields r

instance ShowFields r => Show (Rec r) where
  show r = "Rec{ " ++ intercalate ", " (showFields r) ++ " }"

pattern Rec :: rec
pattern Rec <- (recPluginError -> _) where
  Rec = recPluginError

recPluginError :: a
recPluginError = error "Rec: plugin did not replace use of 'Rec'"

-------------------------------------------------------------------------------
type family (r :: Row') |> (r' :: Row') = (res :: Constraint) | res -> r where
  RNil       |> _  = ()
  RExt n t r |> r' = (Has n r, TypeOf n r ~ t, r |> r')

-------------------------------------------------------------------------------
type family TypeOf (n :: Symbol) (r :: Row') :: Type where
  TypeOf n (RExt n t _) = t
  TypeOf n (RExt _ _ r) = TypeOf n r

class Has (n :: Symbol) (r :: Row') where
  field' :: Lens' (Rec r) (TypeOf n r)

instance {-# overlapping #-} Has n (RExt n t r) where
  field' = lens (\(RE a _) -> a) (\(RE _ r) a -> RE a r)

instance (Has n r, TypeOf n (RExt n' t r) ~ TypeOf n r)
      => Has n (RExt n' t r) where
  field' = extendRecLens' $ field' @n @r

extendRecLens' :: Lens' (Rec r) t -> Lens' (Rec (RExt n t' r)) t
extendRecLens' = flip withLens do
  \g s -> lens (\(RE a r) -> g r) (\(RE a r) b -> RE a (s r b))
